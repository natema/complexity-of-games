---
title: Contributors
layout: default
---

## Admin Contacts:

- [Luciano Gualà](http://www.mat.uniroma2.it/~guala/). Email: {surname}@mat.uniroma2.it
- [Stefano Leucci](https://www.stefanoleucci.com). Email: {name}.{surname}@mpi-inf.mpg.de
- [Emanuele Natale](https://sites.google.com/view/enatale). Email: {surname}@i3s.unice.fr

For technical matters please contact Stefano Leucci or Emanuele Natale.

## Contributors:

- Luciano Gualà
- Stefano Leucci
- Emanuele Natale
- Roberto Tauraso
- Matteo Almanza

Special thanks to [Franceca Marmigi](http://www.francescamarmigi.it/) for the CoG logo. 
